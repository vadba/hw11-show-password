const form = document.querySelector('.password-form');
const showPassword1 = document.querySelector('.icon-password');
const showPassword2 = document.querySelector('.icon-password2');
const passwordInput1 = document.querySelector('.pass1');
const passwordInput2 = document.querySelector('.pass2');
const erroutput = document.querySelector('.erroutput');
const btn = document.querySelector('.btn');

passwordInput1.addEventListener('input', e => {
    if (e.target.value === '') {
        btn.disabled = true;
    } else {
        btn.disabled = false;
    }
});

passwordInput2.addEventListener('input', e => {
    if (e.target.value === '') {
        btn.disabled = true;
    } else {
        btn.disabled = false;
    }
});

const getFormValue = form => {
    const data = new FormData(form);
    const result = {};

    for (const [label, value] of data.entries()) {
        result[label] = value;
    }

    return { ...result };
};

form.addEventListener('submit', e => {
    e.preventDefault();
    if (getFormValue(e.target).pass1 === getFormValue(e.target).pass2) {
        alert('You are welcome');
    } else {
        erroutput.innerText = 'Потрібно ввести однакові значення';
    }
    console.log(getFormValue(e.target).pass1);
});

showPassword1.addEventListener('click', e => {
    if (passwordInput1.getAttribute('type') === 'password') {
        passwordInput1.setAttribute('type', 'text');
        showPassword1.classList.remove('fa-eye');
        showPassword1.classList.toggle('fa-eye-slash');
    } else {
        passwordInput1.setAttribute('type', 'password');
        showPassword1.classList.remove('fa-eye-slash');
        showPassword1.classList.toggle('fa-eye');
    }
});

showPassword2.addEventListener('click', e => {
    if (passwordInput2.getAttribute('type') === 'password') {
        passwordInput2.setAttribute('type', 'text');
        showPassword2.classList.remove('fa-eye');
        showPassword2.classList.toggle('fa-eye-slash');
    } else {
        passwordInput2.setAttribute('type', 'password');
        showPassword2.classList.remove('fa-eye-slash');
        showPassword2.classList.toggle('fa-eye');
    }
});
